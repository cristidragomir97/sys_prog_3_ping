#set (SOURCE_DIR "./")

cmake_minimum_required(VERSION 3.8)
project(sys_prog_3_ping)
#include_directories( BEFORE ${SOURCE_DIR})

add_executable (ping_server ping_server.c)
add_executable (ping_client_1 ping_client_1.c)
add_executable (ping_client_2 ping_client_2.c)
add_executable (ping_client_3 ping_client_3.c)