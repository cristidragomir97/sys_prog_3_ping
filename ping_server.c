#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#define BUFSIZE 1024
#define PORT 9999 


int main(int argc, char **argv)
{
	int sock_fd;		/* socket */
	int port_no;		/* port number */

    // sockaddr_in = ip address type
	struct sockaddr_in serveraddr;
    struct sockaddr_in clientaddr;

    // hostent = structure containing host metadata
    struct hostent *host_info;



    // sets some magic flag to be able to restart the server at will
	int optval = 1;
	setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR,(const void *)&optval, sizeof(int));

    // converts char array pointer to int
	port_no = atoi(argv[1]);
    printf("[-] listening on %d \n",port_no);

    // AF_INET = selects hostname:port format
    // SOCK_DGRAM =  selects UDP packets
	sock_fd = socket(AF_INET, SOCK_DGRAM, 0);

    // The bzero() function erases the data in the n bytes of the memory starting at the location pointed to by s
	bzero((char *)&serveraddr, sizeof(serveraddr));

    // select address:port format
	serveraddr.sin_family = AF_INET;

    // basically 0.0.0.0
    // htonl = converts the format of long int on the machine to the one accepted on the network
	serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);

    // select port
    // htons = converts the format of short int on the machine to the one accepted on the network
	serveraddr.sin_port = htons((unsigned short)PORT);

    // bind socket to address and port
    if(bind(sock_fd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)))
        printf("[error] on binding \n");

    // throw up if shit goes sideways
	if(sock_fd < 0)
        printf("[error] FD:%d \n",sock_fd);

    // compute client length 
    int clientlen = sizeof(clientaddr);

    // do this forever
    while (1) {
        
		char *buf = malloc(sizeof(char) * (BUFSIZE + 1)) ; // allocates buffer size
        int n; // size of the recieved message

       
        
        memset(buf,'\0', BUFSIZE);
        // recvfrom: receive a UDP datagram from a client
		n = recvfrom(sock_fd, buf, BUFSIZE, 0,(struct sockaddr *)&clientaddr, &clientlen);

        // if error code in what was recieved from the clleint
		if (n < 0)
			perror("[error] recieving datagram from client");
        


        // gethostbyaddr =  determine who sent the datagram
        // find out shit about the client, in order to be able to echo message
        host_info = gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr, // save client addres 
				            sizeof(clientaddr.sin_addr.s_addr), // something something, length of the buffer
				            AF_INET); // format thingy

        if (host_info == NULL)
			perror("[error] on getting client info");

        // convert ip addres to dotted format
		char *hostaddr = inet_ntoa(clientaddr.sin_addr);

		if (hostaddr == NULL)
			perror("[error] on getting client ip \n");

        printf("[*] echoing message to %s",hostaddr);

		// sendto: echo the input back to the client 
		n = sendto(sock_fd, buf, n, 0,(struct sockaddr *)&clientaddr, clientlen);
		
   
        // if error code in sending message 
        if (n < 0)
			perror("[error] on sending echo message");
    }

    return 0;
}

