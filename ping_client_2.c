/*
    Simple udp client
*/
#include<stdio.h> //printf
#include<string.h> //memset
#include<stdlib.h> //exit(0);
#include<arpa/inet.h>
#include<sys/socket.h>
#include<time.h>


#define SERVER "127.0.0.1"
#define BUFLEN 1024  //Max length of buffer
#define PORT 9999   //The port on which to send data



static double TimeSpecToSeconds(struct timespec* ts)
{
    return (double)ts->tv_sec + (double)ts->tv_nsec / 1000000000.0;
}

int main()
{
    struct sockaddr_in socket_in;
    struct timespec send_time;
    struct timespec recv_time;
    
    struct timeval timeout;      
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;

    double elapsedSeconds;

    int s, i;
    int slen=sizeof(socket_in);
    char buf[BUFLEN];
    char str[20] = "this";
 
    if ( (s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
       perror("[*] error connecting to socket");
    }
 

    //set socket timeout
    if (setsockopt (s, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout)) < 0)
        perror("[*] failed to set timeout on socket_fd\n");


    memset((char *) &socket_in, 0, sizeof(socket_in));
    socket_in.sin_family = AF_INET;
    socket_in.sin_port = htons(PORT);

     
    if (inet_aton(SERVER , &socket_in.sin_addr) == 0) 
    {
        fprintf(stderr, "inet_aton() failed\n");
        exit(1);
    }
 

    //send the message
    if (sendto(s, str, strlen(str) , 0 , (struct sockaddr *) &socket_in, slen)==-1)
    {
              perror("[*] error connecting to socket");
    }else{
         if(clock_gettime(CLOCK_MONOTONIC_RAW, &send_time)){
             perror("[*] error getting send_time");
         }
    }
         
    //clear the buffer by filling null, it might have previously received data
    memset(buf,'\0', BUFLEN);

    //try to receive some data, this is a blocking call
    if (recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &socket_in, &slen) == -1){
             perror("[*] message has timed out");
             exit(1);
    }else{
        if(clock_gettime(CLOCK_MONOTONIC_RAW, &recv_time)){
                perror("[*] error getting recv_time");
        }
    }
        printf("[*] buffer contents : %s \n",buf);

    
    elapsedSeconds = TimeSpecToSeconds(&recv_time) - TimeSpecToSeconds(&send_time);

    printf("[*] RTT: %fs \n",elapsedSeconds);

    close(s);
    return 0;
}