#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<time.h>

#define SERVER "127.0.0.1"
#define BUFLEN 10
#define PORT 9999


static double timespec_to_sec(struct timespec* ts){
    return (double)ts->tv_sec + (double)ts->tv_nsec / 1000000000.0;
}

int main(){
    unsigned int i = 0;
    int s;

    struct sockaddr_in socket_in;
    struct timeval timeout;

    timeout.tv_sec = 1;
    timeout.tv_usec = 0;


    if((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        perror("[*] error connecting to socket");



    // clear structure
    memset((char *) &socket_in, 0, sizeof(socket_in));

    // select hostname:port addressing mode
    socket_in.sin_family = AF_INET;

    socket_in.sin_port = htons(PORT);

    //set socket timeout
    if (setsockopt (s, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout,sizeof(timeout)) < 0)
        perror("[*] failed to set timeout on socket_fd\n");


    if(inet_aton(SERVER , &socket_in.sin_addr) == 0){
        perror("[*] error encoding ip address");
        exit(1);
    }

    int slen=sizeof(socket_in);

    while (1) {
        struct timespec send_time;
        struct timespec recv_time;

        char buf[BUFLEN];

        char this_seq[20];
        int n = sprintf(this_seq, "%d",i);

        if(sendto(s, this_seq, strlen(this_seq), 0, (struct sockaddr *) &socket_in, slen)==-1){
            perror("error sending message");
        }else{
            if(clock_gettime(CLOCK_MONOTONIC_RAW, &send_time)){
                perror("error getting send_time");
                return 1;
            }
        }

        memset(buf,'\0',strlen(this_seq));

        if(recvfrom(s, buf, strlen(this_seq) , 0, (struct sockaddr *) &socket_in, &slen) == -1){
            printf("Packet %u: lost.\n",i);

        }else{
            if(clock_gettime(CLOCK_MONOTONIC_RAW, &recv_time)){
                perror("[*] error getting recv_time");
                return 1;
            }else{
                int recieved_seq = atoi(buf);
                if(recieved_seq == i){
                    double rtt =  timespec_to_sec(&recv_time) - timespec_to_sec(&send_time);
                    printf("Packet %d: %fs\n",i,rtt);
                }else{
                    printf("Packet %d: wrong counter! Received %u instead of %u.",i,recieved_seq,i);
                }


            }
        }

        i++;
        sleep(1);
    }
    close(s);
    return 0;
}

